function doHomework(subject, callback) {
    alert(`Starting my ${subject} homework.`);

    callback();
}

function alertFinished(){
    alert('Finished my homework');
}

doHomework('math', alertFinished);


function cb() {
    console.log('Hey Im callback function');
}

navigator.geolocation.getCurrentPosition(cb);

function getCurrentPosition2(cbFunc) {
    cbFunc();
}


getCurrentPosition2(cb);

// Hey Im callback function
// Hey Im callback function